const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Schoolboard = require('../models/schoolboard');

// New Schoolboard
router.post('/newschoolboard', (req, res, next) => {
    let newSchoolboard = new Schoolboard ({
        name: req.body.name,
        image: req.body.image,
        link: req.body.link
    });

    Schoolboard.addSchoolboard(newSchoolboard, (err, schoolboard) => {
        if(err) {
            console.error("Error: Adding Schoolboard Failed");
            res.json({success: false, msg: 'Failed to add schoolboard'});
        } else {
            console.log("Success: Adding Schoolboard Succeded");
            res.json({success: true, msg: 'Schoolboard Added'});
        }
    });
});

// Get Schoolboards
router.get('/getschoolboards', (req, res, next) => {
    Schoolboard.getSchoolboards(null, (err, schoolboards) => {
        if(err) throw err;
        if(!schoolboards) {
            return res.json({success: false, msg: 'No schoolboards found!'});
        }
        else{

            var schoolboardsMap = {};

            schoolboards.forEach(function(schoolboard) {
                schoolboardsMap[schoolboard._id] = schoolboard;
            });

            res.send(schoolboardsMap);
        }
    });
});

module.exports = router;
