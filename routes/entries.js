const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Entry = require('../models/entry');

// New Entry
router.post('/newentry', (req, res, next) => {
    let newUser = new Entry ({
        hours: req.body.hours,
        name: req.body.name,
        recipient: req.body.recipient,
        recipientid: req.body.recipientid,
        date: req.body.date
    });

    Entry.addEntry(newUser, (err, entry) => {
        if(err) {
            console.error("Error: Adding Entry Failed");
            res.json({success: false, msg: 'Failed to add entry'});
        } else {
            console.log("Success: Adding Entry Succeded");
            res.json({success: true, msg: 'Entry Added'});
        }
    });
});

// Get User Hours
router.get('/getuserhours', (req, res, next) => {
    const recipientid = req.query.recipientid;

    Entry.getEntriesByRecipientID(recipientid, (err, entries) => {
        if(err) throw err;
        if(!entries) {
            return res.json({success: false, msg: 'No entries found!'});
        }
        else{

            var entryMap = {};

            entries.forEach(function(entry) {
                entryMap[entry._id] = entry;
            });

            res.send(entryMap);
        }
    });
});

module.exports = router;
