# Volunteer Hours Tracker

Track Students Volunteer Hours

### Version
1.0.1

## Usage


### Installation

Install the dependencies

```sh
$ npm install
```

Add your database info

```sh
module.exports = {
  database: 'mongodb://localhost:27017/yourdatabase',
  secret: 'yoursecret'
}
```

Run app

```sh
$ npm start
```
