import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {HttpParams} from "@angular/common/http";
import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {
  authToken: any;
  user: any;
  entry: any;

  constructor(private http:Http) { }

  registerUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('users/register', user, {headers: headers})
      .map(res => res.json());
  }

  authenticateUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('users/authenticate', user, {headers: headers})
      .map(res => res.json());
  }

  getProfile() {
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('users/profile', {headers: headers})
      .map(res => res.json());
  }

  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn() {
    return tokenNotExpired('id_token');
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  newEntry(entry) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('entries/newentry', entry, {headers: headers}).map(res => res.json());
  }

  getEntries(recipientid) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('entries/getuserhours', { headers: headers, params: { recipientid: recipientid } }).map(res => res.json());
  }

  newSchoolboard(schoolboard){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('schoolboards/newschoolboard', schoolboard, {headers: headers}).map(res => res.json());
  }

  getSchoolboards(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('schoolboards/getschoolboards', { headers: headers }).map(res => res.json());
  }

}
