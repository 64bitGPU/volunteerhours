import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  name: String;
  username: String;
  email: String;
  password: String;
  schoolboard: String;

  dropdownOptions: String[] = [];

  dropdownConfig = {
    displayKey:"description", //if objects array passed which key to be displayed defaults to description
    search: true, //true/false for the search functionlity defaults to false,
    height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
    placeholder:'Enter Schoolboard', // text to be displayed when no item is selected defaults to Select,
    moreText: 'more', // text to be displayed whenmore than one items are selected like Option 1 + 5 more
    noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
    searchPlaceholder: 'Enter Your Schoolboard', // label thats displayed in search input,
    limitTo: 5
  };

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService) { }

  ngOnInit() {
    this.getSchoolboards();
  }

  getSchoolboards(){
    var result = [];

    this.authService.getSchoolboards().subscribe(data => {
      for (var counter in data) {
        result.push(data[counter].name);
      }
    });

    console.log(result);

    this.dropdownOptions = result;

    console.log(this.dropdownOptions);

  }

  addSchoolboard(){
    const schoolboard = {
      name: "TVDSB",
      image: "https://www.tvdsb.ca/en/images/structure/logo-print.svg",
      link: "https://www.tvdsb.ca/"
    }

    this.authService.newSchoolboard(schoolboard).subscribe(data => {
      if(data.success) {
        this.flashMessage.show('Schoolboard Added', {cssClass: 'alert-success', timeout: 3000});
      } else {
        this.flashMessage.show('Schoolboard Not Added', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }

  addEntry() {
    const entry = {
      hours: 22,
      name: "Jane",
      recipient: "Bob",
      recipientid: "5c2fc0df1d6948190cd1e6d2",
      date: new Date('December 17, 1995 03:24:00')
    };

    // Add Entry
    this.authService.newEntry(entry).subscribe(data => {
      if(data.success) {
        this.flashMessage.show('Entry Added', {cssClass: 'alert-success', timeout: 3000});
      } else {
        this.flashMessage.show('Entry Not Added', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }

  onRegisterSubmit() {
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password,
      schoolboard: this.schoolboard
    };

    // Required Fields
    if(!this.validateService.validateRegister(user)) {
      this.flashMessage.show('Please fill in all fields', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    // Validate Email
    if(!this.validateService.validateEmail(user.email)) {
    this.flashMessage.show('Please use a valid email', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    // Register user
    this.authService.registerUser(user).subscribe(data => {
      if(data.success) {
        this.flashMessage.show('You are now registered and can now login', {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/login']);
      } else {
        this.flashMessage.show('Something went wrong', {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/register']);
      } 
    });
  }
}