import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef, ViewChild, Injector, EmbeddedViewRef } from '@angular/core';
import { EntrycardComponent } from '../entrycard/entrycard.component';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  totalhours: string

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver, 
    private viewContainerRef: ViewContainerRef, 
    private validateService: ValidateService,
    private authService: AuthService,
    private router: Router,
    private injector: Injector,
    private flashMessage: FlashMessagesService) { }

  loadCards() {
    this.authService.getProfile().subscribe(profile => {
      console.log(profile.user._id);
      this.authService.getEntries(profile.user._id).subscribe(data => {

        var hourCounter = 0;
        var columnCounter = 1;

        for (var counter in data) {
          const factory = this.componentFactoryResolver.resolveComponentFactory(EntrycardComponent);
          const ref = this.viewContainerRef.createComponent(factory);
          ref.instance.name = data[counter].name;
          ref.instance.hours = data[counter].hours;

          var datePipe = new DatePipe('en-US');
          ref.instance.date = datePipe.transform(data[counter].date, 'dd/MM/yyyy');

          ref.changeDetectorRef.detectChanges();

          const domElem = (ref.hostView as EmbeddedViewRef<any>)
            .rootNodes[0] as HTMLElement;

          if(columnCounter == 1){
            document.getElementById("col1").appendChild(domElem);
            columnCounter += 1;
          } 
          else if(columnCounter == 2){
            document.getElementById("col2").appendChild(domElem);
            columnCounter += 1;
          }
          else if(columnCounter == 3){
            document.getElementById("col3").appendChild(domElem);
            columnCounter = 1;
          }
          hourCounter += data[counter].hours;
        }
        this.totalhours = hourCounter.toString();

        console.log(hourCounter);
      });
    });
  }

  ngOnInit() {
    this.loadCards();
  }
}
