import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-entrycard',
  templateUrl: './entrycard.component.html',
  styleUrls: ['./entrycard.component.css']
})
export class EntrycardComponent implements OnInit {
  name: string;
  hours: number;
  date: string;

  constructor() { }

  ngOnInit() {
  }
}
