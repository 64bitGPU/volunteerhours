const config = require('../config/database');
const mongoose = require('mongoose');

// Schoolboard Schema
const SchoolboardSchema = mongoose.Schema ({
  name: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  link: {
    type: String,
    required: true
  }
});

const Schoolboard = module.exports = mongoose.model('Schoolboard', SchoolboardSchema);

module.exports.getSchoolboardById = function(id, callback) {
    Schoolboard.findById(id, callback);
}

module.exports.getSchoolboards = function(req, callback) {
    const query = {};
    Schoolboard.find(query, callback);
}

module.exports.addSchoolboard = function(newSchoolboard, callback) {
    newSchoolboard.save(callback);
}