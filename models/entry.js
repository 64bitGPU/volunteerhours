const config = require('../config/database');
const mongoose = require('mongoose');

// Entry Schema
const EntrySchema = mongoose.Schema ({
  hours: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  recipient: {
    type: String,
    required: true
  },
  recipientid: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  }
});

const Entry = module.exports = mongoose.model('Entry', EntrySchema);

module.exports.getEntryById = function(id, callback) {
  Entry.findById(id, callback);
}

module.exports.getEntriesByRecipientID = function(recipientid, callback) {
    const ObjectId = require('mongoose').Types.ObjectId; 
    const query = { recipientid: new ObjectId(recipientid) };

    Entry.find(query, callback);
}

module.exports.addEntry = function(newEntry, callback) {
    newEntry.save(callback);
}